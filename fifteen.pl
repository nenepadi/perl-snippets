#!/usr/bin/perl -w

%count = ();
@words = ();

print "Enter a list of strings. Press Ctrl+D to quit standard input: \n";
chomp(@words = <STDIN>);

foreach $word(@words){
    $count{$word} += 1;
}

foreach $word(sort keys %count){
    $time = " ";
    if($count{$word} > 1){
	$time = "times";
    } else{
	$time = "time";
    }
    
    print "$word was found $count{$word} $time.\n";
}
