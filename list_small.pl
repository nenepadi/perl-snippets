#!/usr/bin/perl -w

$i = 0;
@small_list = ();

if(open(SMALL_FILE, "small.txt")){
    $line = <SMALL_FILE>;
    while($line cmp ""){
#        print($line);
	$small_list[$i] = $line;
	$line = <SMALL_FILE>;
	$i++;
    }
}

close(SMALL_FILE);

print reverse sort(@small_list);
