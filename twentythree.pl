#!/usr/bin/perl -w
sub numberToWord{
    my %mapped_data;
    @mapped_data{1..9} = qw(one two three four five six seven eight nine);

    my($num) = @_;
    if($mapped_data{$num}){
	return $mapped_data{$num};
    } else{
	return $num;
    }
}

print "Enter first number: ";
chomp($first = <STDIN>);
print "Enter second number: ";
chomp($second = <STDIN>);

$message = &numberToWord($first) . " plus " . 
    &numberToWord($second) . " equals " .
    &numberToWord($first+$second) . ".\n";

print $message;
