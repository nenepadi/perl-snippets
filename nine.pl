#!/usr/bin/perl -w

print "What is the temperature outside: ";
$temperature = <STDIN>;
print "\n";

if($temperature > 75){
    print "The weather is too hot.\n";
} elsif($temperature >= 68 && $temperature <= 75){
    print "The weather is just right.\n";
} else{
    print "The weather is too cold.\n";
}
