#!/usr/bin/perl -w

print "Input as many list of strings as you want.\n Press the enter key to input another.\n Press Ctrl+D to quit from standard input: \n";
@array_of_str = <STDIN>;

print "\nWhich of the strings do you want us to print back to you: ";
$index = <STDIN>;
chomp($index);

print "\nYou asked us to print back: \n" . $array_of_str[$index-1];
