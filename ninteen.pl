#!/usr/bin/perl -w

print "Enter a list of strings. Press Ctrl+D to quit standard input: \n";
chomp(@strings = <STDIN>);

print "Enter column width: ";
chomp($width = <STDIN>);

$formated = "%" . $width . "s\n";
printf "\n" . $formated x @strings, @strings;
