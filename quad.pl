#!/usr/bin/perl -w

print "Enter a: ";
chomp($a = <STDIN>);
print "Enter b: ";
chomp($b = <STDIN>);
print "Enter c: ";
chomp($c = <STDIN>);

$x1 = (-$b - sqrt($b ** 2 - (4 * $a * $c))) / (2 * $a);
$x2 = (-$b + sqrt($b ** 2 - (4 * $a * $c))) / (2 * $a);

print "$x1, $x2\n";
