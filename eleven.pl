#!/usr/bin/perl -w

$i = 1;
$sum = 0;
open(SMALL_FILE, "small.txt");

while($i < 999){
    $line = <SMALL_FILE>;
    $sum += $i;
    $i++;
}

print "\nThe " . $i . "th line is: \n" . $line;
print "\nThe total is " . $sum . "\n";

close(SMALL_FILE);
