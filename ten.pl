#!/usr/bin/perl -w

print "Enter a list of numbers. Remember 999 is a control number: \n";

#initializing the sum variable and 
#the variable that stores the list of numbers...
$num = 0;
$sum = 0;
$count = 0;
@numbers = ();
#checking for control number 999...
while($num != 999){
    $num = <STDIN>;
    chomp($num);
    $numbers[$count] = $num;
    $count++; 
}

for($i = 0; $i < (@numbers-1); $i++){
    $sum += $numbers[$i];
}

#printing results...
print "The sum of all the numbers is " . $sum . "\n";
