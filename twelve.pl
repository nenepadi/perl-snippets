#!/usr/bin/perl -w

#declaring the index and array variables
print "Name as many terminal commands that you know: \n";
@array_of_str = <STDIN>;
$index = @array_of_str;

#reversing elements in the array...
print "\nPrinting your ideas back to you in the reverse order as you gave.\n";
until($index == -1){
    #printing array...
    print $array_of_str[$index];
    $index--; 
}
