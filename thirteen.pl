#!/usr/bin/perl -w

$j = 0;

print "Printing some of the numbers from 0-32.\n";
print "Number" . "\t" . "Square \n";
print "------" . "\t" . "------ \n";
while($j <= 32){
    $square2 = $j ** 2;
    print $j . "\t" . $square2 . "\n";
    $j = $j + int(rand(5));
}

print "\n";

print "Printing all the numbers from 0 - 32.\n";
print "Number" . "\t" . "Square \n";
print "------" . "\t" . "------ \n";
for($i = 0; $i <= 32; $i++){
    $square = $i ** 2;
    print $i . "\t" . $square . "\n";
}
