#!/usr/bin/perl -w

print "Enter the first number: ";
$num1 = <STDIN>;
chomp($num1);

print "Enter the second number: ";
$num2 = <STDIN>;
chomp($num2);

$product = $num1 * $num2;

print "\n";
print "The product of $num1 and $num2 is $product. \n";

