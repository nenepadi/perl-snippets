#!/usr/bin/perl -w

print "enter a list \n";

chomp(@files = <STDIN>);

foreach $file(@files){
	print "$file is readable\n" if -r $file;
	print "$file does not exist\n" unless -e $file;
	print "$file is writeable\n" if -w $file;
	print "$file is executable\n" if -x $file;
}
