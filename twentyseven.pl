#!/usr/bin/perl -w

print "Enter a file name to see how how it has been in the system.",
    "Press Ctrl+D to quit: \n";
$oldest_age = 0;
$age = 0;
while(<>){
    chomp;
    $age = -M;
    if($oldest_age < $age){
	$oldest_name = $_;
	$oldest_age = $age;
    }
}

print "The oldest file is $oldest_name ",
    "and is $oldest_age days old.\n";
