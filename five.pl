#!/usr/bin/perl -w

print "Name as many terminal commands that you know.Press Ctrl+D to quit from standard input: \n";
#reading standard input into an array...
@array_of_commands = <STDIN>; 

#printing array...
print "\nPrinting your ideas back to you in the reverse order as you gave.\n";
print reverse(@array_of_commands);
