#!/usr/bin/perl -w
sub numberToWord{
    my %mapped_data;
    @mapped_data{1..9} = qw(one two three four five six seven eight nine);

    my($num) = @_;
    if($mapped_data{$num}){
	   return $mapped_data{$num};
    } else{
	   return $num;
    }
}

print "Enter a number to see it in words: \n";
while(<STDIN>){
    chomp;
    print "$_ to word is " . &numberToWord($_) . "\n";
}
