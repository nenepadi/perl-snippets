#!/usr/bin/perl -w

if(@ARGV == 0){
    print "Enter list words as input. Press Ctrl+D to quit standard input:\n";
    chomp(@list_str = <>);
    print "\nThe words that contain all vowels are: \n";
    foreach (@list_str){
	if (/a.*e.*i.*o.*u/i){
	    print $_ . "\n";
        }
    }
} else{
    while(<>){
        print if(/a.*e.*i.*o.*u/i);
    }
}

