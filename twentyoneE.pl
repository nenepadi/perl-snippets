#!/usr/bin/perl -w

while(<>){
    chomp;
    ($file) = (split /:/)[4];
    if($file eq ""){
	$real = " ";
    } else{
	($real) = split(/,/, $file);
	($first) = split(/\s+/, $real);
	$seen{$first}++;
    }
}

foreach(keys %seen){
    if($seen{$_} > 1){
	print "$_ was seen $seen{$_} times.\n";
    }
}
