#!/usr/bin/perl -w

srand; #this initializes the seed for the rand function...

print "Please enter a list of strings. Press Ctrl+D to quit from standard input: \n";
@list_of_str = <STDIN>;

#Printing randomly an element from the list...
print "\nPrinting an element randomly...\n";
print $list_of_str[int(rand(@list_of_str))]; 
