#!/usr/bin/perl -w

if(@ARGV == 0){
    print "Enter list words as input. Press Ctrl+D to quit standard input:\n";
    chomp(@list_str = <>);
    print "\nThe words that contain all vowels are: \n";
    foreach (@list_str){
	if (/^[^aeiouAEIOU]*a[^eiouEIOU]*e[^aiouAIOU]*i[^aeouAEOU]*o[^aeiuAEIU]*u[^aeioAEIO]*$/){
	    print $_ . "\n";
        }
    }
} else{
    while(<>){
        print if(/^[^aeiou]*a[^eiou]*e[^aiou]*i[^aeou]*o[^aeiu]*u[aeio]*$/);
    }
}

