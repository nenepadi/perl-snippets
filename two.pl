#!/usr/bin/perl -w

$pi = 3.1429;

print "Please enter a radius to calculate a circumference: ";
$radius = <STDIN>;
chomp($radius);

$circumference = 2 * $pi * $radius;

print "The circumference is $circumference.\n";
