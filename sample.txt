With cumulative SYN/ACKs, the receiver can only acknowledge a segment if all previous segments have been received

With cumulative SYN/ACKs, receiver cannot selectively acknowledge blocks of segments: 
e.g., SYN/ACK for S0-S3 and S5-S7 (but not for S4)


Note: The use of cumulative SYN/ACKs imposes constraints on the retransmission schemes:
In case of an error, the sender may need to retransmit all data that has not been acknowledged
*****************************************************************************


