#!/usr/bin/perl -w

%hashed_data = ("Input"=>"Output", "Red"=>"Apple", "Green"=>"Leaves", "Blue"=>"Ocean");

print "Please what is the key to the element: ";
$key = <STDIN>;
chomp($key);

print "\n";

print $key . " maps to " . $hashed_data{$key} . "\n";
