#!/usr/bin/perl -w

while(<>){
    chomp;
    ($user, $file) = (split /:/)[0,4];
    if($file eq ""){
	$real = " ";
    } else{
	($real) = split(/,/, $file);
	($first) = split(/\s+/, $real);
	$names{$first} .= " $user";
    }
}

foreach(keys %names){
    $this = $names{$_};
    if($this =~ /. /){
	print "$_ used by: $this.\n";
    }
}
