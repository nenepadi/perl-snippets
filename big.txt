I am big.txt

TCP receivers use acknowledgments (ACKs) to confirm the receipt of data to the sender
Acknowledgment can be added (“piggybacked”) to a data segment that carries data in the opposite direction
ACK information is included in the the TCP header
Acknowledgements are used for flow control, error control, and congestion control

TCP uses sequence numbers to keep track of transmitted and acknowledged data
Each transmitted byte of payload data is associated with a sequence number
Sequence numbers count bytes and not segments
Sequence number of first byte in payload is written in SeqNo field 
Sequence numbers wrap when they reach 232-1 

The sequence number of the first sequence number (Initial sequence number) is negotiated during connection setup

