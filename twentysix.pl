#!/usr/bin/perl -w

print "Enter a filename to see whether it is readable, writable or executable: \n";
while(<>){
    chomp;
    print "$_ is readable\n" if -r;
    print "$_ is writable\n" if -w;
    print "$_ is executable\n" if -x;
    print "$_ does not exist\n" unless -e;

    print "\n";
}
