#!/usr/bin/perl -w

$pi = 3.1429;
$radius = 12.5;

$circumference = 2 * $pi * $radius;

print "The circumference is $circumference.\n";
