#!/usr/bin/perl -w

print "Enter an input file: ";
chomp($input_file = <STDIN>);
print "Enter the name of an output file: ";
chomp($output_file = <STDIN>);
print "Enter a search string: ";
chomp($search = <STDIN>);
print "Enter a replacement string: ";
chomp($replace = <STDIN>);

open(IN, "$input_file") || die "cannot open $input_file: $!";
die "will not overwrite $output_file" if -e $output_file;

open(OUT,"> $output_file") || die "cannot create $output_file: $!";

while(<IN>){
    s/$search/$replace/g;
    print OUT $_;
}

close(IN);
close(OUT);
